var child_process = require("child_process");
var fs = require("fs");
var parser = new (require("fast-xml-parser")).XMLParser();

child_process.execSync(`wget -nv http://map.syn3h.com.s3.amazonaws.com/ -O ListBucketResult.xml`);

var xml = fs.readFileSync(`ListBucketResult.xml`, 'utf8');
var jso = parser.parse(xml);

var urls = jso.ListBucketResult.Contents.map(x => "http://map.syn3h.com.s3.amazonaws.com/" + x.Key).filter(x => !x.endsWith("/"));

fs.writeFileSync("urllist.txt", urls.join("\n"));

child_process.spawnSync(`wget`, ["-nv", "-x", "-N", "-i", "urllist.txt"], {stdio:"inherit"});
